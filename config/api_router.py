from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from ecommerce.users.api.views import UserViewSet
from ecommerce.shop.api.views import ProductViewSet, OrderViewSet, OrderItemViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("products", ProductViewSet,basename="product")
router.register("orders", OrderViewSet,basename="order")
router.register("cart", OrderItemViewSet,basename="cart")

app_name = "api"
urlpatterns = router.urls
