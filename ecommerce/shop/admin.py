from django.contrib import admin
from ecommerce.shop.models import Product, Order, OrderItem, Category, Address
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ("title", "price", "describe", "slug", "category", "picture")
    list_filter = ("title","category")
    search_fields = ('title','category')
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Product, ProductAdmin)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(Category)
admin.site.register(Address)
