from rest_framework import serializers
from ecommerce.shop.models import Product, Order, OrderItem
from ecommerce.users.api.serializers import UserSerializer



class ProductSerializers(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id','title', 'price', 'picture','slug','category','describe')


class OrderItemSerializers(serializers.ModelSerializer):
    #serialzer interger
    item_id = serializers.IntegerField()
    class Meta:
        model = OrderItem
        fields = ('item_id','quantity')


class OrderSerializers(serializers.ModelSerializer):

    items = OrderItemSerializers(many=True)

    class Meta:
        model = Order
        fields = ('user','create_at','items', 'being_delivered')

    def create(self, validated_data):
        items_data = validated_data.pop('id')
        order = Order.objects.create(**validated_data)
        for item_data in items_data:
            OrderItem.objects.create(order=order, **item_data)
        return order
