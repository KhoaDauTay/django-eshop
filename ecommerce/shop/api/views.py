from rest_framework import viewsets,permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from .serializers import OrderSerializers, ProductSerializers, OrderItemSerializers
from ecommerce.shop.models import Product, Order, OrderItem

User = get_user_model()
class ProductViewSet(viewsets.ModelViewSet):

    queryset = Product.objects.all()
    serializer_class = ProductSerializers
    permission_classes = (IsAuthenticated,)

class OrderViewSet(viewsets.ViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializers

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=id):
        order = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(order)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class OrderItemViewSet(viewsets.ViewSet):

    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializers

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(
            serializer.data
            )

    def retrieve(self, request, pk=id):
        order = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(order)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # item = get_object_or_404(Product, id=request.data.get("product_id"))
        # order_item, created = OrderItem.objects.get_or_create(
        #     item =item,
        #     user = request.user,
        #     ordered = False,
        # )
        product_id = int(request.data.get("item_id"))
        item = Product.objects.get(id = product_id)
        if item.id == product_id:
            order_qs = OrderItem.objects.get(user = request.user,item = item)
            quantity = int(request.data["quantity"]) + order_qs.quantity
            order_qs.quantity = quantity
            order_qs.save()
            serializer = self.serializer_class(order_qs)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def partial_update(self, request, *args, **kwargs):
        instance = self.queryset.get(pk=kwargs.get('pk'))
        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
