from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = "ecommerce.shop"
