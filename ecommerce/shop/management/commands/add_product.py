import json
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from rest_framework.views import APIView
from rest_framework.response import Response
from urllib.request import urlopen
from ecommerce.shop.models import Product

class Command(BaseCommand):


    help = 'Create default product'

    def handle(self, *args, **kwargs):

        json_data = open('json/product.json')
        data1 = json.load(json_data) # deserialises it
        products = Product.objects.all()
        for product in data1:
            if product['id'] not in [i.id for i in products]:
                Product.objects.create(
                    id=product['id'],
                    title=product['title'],
                    price=product['price'],
                    picture=product['picture'],
                    slug=product['slug'],
                    category=product['category'],
                    describe=product['describe'])
                if Product.objects.count() == 5:
                    print("Add Data default success")
            else :
                print("Duplicate data")
                print(f"Id = {product['id']} is already in the Product object")


