from django.urls import path
from ecommerce.shop.views import *
from ecommerce.shop.api.views import *
app_name = "shop"
urlpatterns = [
    path("", ShopView.as_view(), name="index"),
    path('order-summary/', OrderSummaryView.as_view(), name='order-summary'),
    path('invoice/', ListInvoice.as_view(), name='list-invoice'),
    path('check-out/', check_out_order, name='check-out'),
    path('add-to-cart/<slug:slug>/', add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<slug:slug>/', remove_from_cart, name='remove-from-cart'),
    path('check-address/', check_address, name='address'),
    path('<slug:slug>/', ItemDetailView.as_view(), name='product'),

]
