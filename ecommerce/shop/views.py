from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.generic import ListView, DetailView, View
from ecommerce.shop.models import Product, Order, OrderItem, Address, Category
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.


class ShopView(ListView):
    model = Product
    paginate_by = 12
    ordering = ['id']
    template_name = "shop/product.html"

class ItemDetailView(DetailView):

    model = Product
    template_name = "shop/product-detail.html"


class OrderSummaryView(LoginRequiredMixin,View):

    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, being_delivered=False)
            context = {
                'object': order
            }
            return render(self.request, 'shop/cart.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")


@login_required
def add_to_cart(request,slug):

    item = get_object_or_404(Product, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item =item,
        user = request.user,
        ordered = False
    )
    order_history = Order.objects.filter(user=request.user, being_delivered=False)
    if order_history.exists():
        order = order_history[0]
        if order.items.filter(item__slug = item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "This item quantity was updated.")
            return redirect("shop:order-summary")
        else:
            order.items.add(order_item)
            messages.info(request, "This item was added to your cart.")
            return redirect("shop:order-summary")
    else:
        order = Order.objects.create(user=request.user)
        order.items.add(order_item)
        messages.info(request, "This item was added to your cart.")
        return redirect("shop:order-summary")

@login_required
def remove_from_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        being_delivered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            messages.info(request, "This item was removed from your cart.")
            return redirect("shop:order-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("shop:product", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("shop:product", slug=slug)



class ListInvoice(ListView):
    model = Order
    template_name = 'shop/invoice.html'

@login_required
def check_address(request):
    return render(request, 'shop/form-address.html')

def check_out_order(request):
    if request.method == 'POST':
        city = request.POST.get("City")
        province = request.POST.get("Province")
        street = request.POST.get("Street")
        zip_code = request.POST.get("Zip")

        address = Address(
            user= request.user,
            country=city,
            street_address=street,
            apartment_address=province,
            zip=zip_code,
        )
        address.save()
        # order_bill.shipping_address.add(address)
        # order_bill.being_delivered = True
        # order_bill.save()
        order_bill = Order.objects.get(user= request.user, being_delivered=False)
        order_bill.shipping_address = address
        order_bill.save()
        context = {
            'object' : order_bill,
            'order_item'  : order_bill.items.all()
        }
        return render(request, 'shop/checkout.html', context = context)
    else:
         return redirect("/")
